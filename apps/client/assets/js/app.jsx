import React from 'react';

const App = (props) => {
    return (
        <h1>Hi {props.name}</h1>
    )
};

export default App;